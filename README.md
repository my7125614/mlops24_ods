Repo for MLOps track from ods.ai

To build container run:
```
docker build -t test1 .
```

To run built container run:
```
docker run -it test1
```